# Game of Life
The Game of Life is a cellular automaton devised by the British mathematician John Horton Conway in 1970.

The "game" is a zero-player game, meaning that its evolution is determined by its initial state, requiring no further input. One interacts with the Game of Life by creating an initial configuration and observing how it evolves.

The universe of the Game of Life is an infinite two-dimensional orthogonal grid of square cells, each of which is in one of two possible states, alive or dead, or "populated" or "unpopulated". Every cell interacts with its eight neighbors, which are the cells that are horizontally, vertically, or diagonally adjacent. At each step in time, the following transitions occur:

* Any live cell with fewer than two live neighbors dies, as if caused by underpopulation.
* Any live cell with two or three live neighbors lives on to the next generation.
* Any live cell with more than three live neighbors dies, as if by overpopulation.
* Any dead cell with exactly three live neighbors becomes a live cell, as if by reproduction.

Reference: [Conway's Game of Life](https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life) on Wikipedia.

## Functional Requirements
* The user should be able to set the initial configuration using:
  1. The mouse click event directly on the page;
  2. Some predefined boards;
  3. A specific board introduced using an API (common format bellow).
* The user should be able to simulate generations using:
  1. A button to iterate one generation at a time;
  2. A button to iterate N generations of a time.
      * The user can choose the N factor.
      * The user can choose the delay between the display of each generation (it can be 0 delay).
      * The user can choose to run the simulation forever until it is over.

The board's common format is **an array of binary arrays**.

So, for example, a board of size 3 (3x3) in the common format would be: ```[[0, 1, 0], [1, 1, 1], [0, 1, 0]]```.

## Getting started
These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Installing
After cloning this repository, enter its root folder and type ```npm install``` in a terminal window. This will download and install all the required packages to make this application functional.

### Running
Open a terminal window and type ```npm start```. This will launch the application in a new browser's tab.

For every change you make in this project, the opened browser window will automatically update.

#### Environment config
In order to make this app fully operational, you must create an ```.env``` file containing your ```REACT_APP_API_URL``` variable.

Obs: this app is currently using [this project's API](https://gitlab.com/oriworks/fe-tech-unit-react) for testing purposes.

## Built with
* [React](https://reactjs.org/) – JavaScript library for UI
* [Create React App](https://create-react-app.dev/) – the React web app bundler 
* [Sass](https://sass-lang.com/) – most common CSS extension language
* [Styled Components](https://styled-components.com/) – a CSS-in-JS library

## Contributing
Please feel free to propose new features by raising an [Issue](https://gitlab.com/eduardobasso/game-of-life-react/-/issues/new) or [Forking this repository](https://gitlab.com/eduardobasso/game-of-life-react/-/forks/new) and creating a Pull Request.

## Authors
* **Eduardo Basso** – *Initial work* – [eduardobasso](https://gitlab.com/eduardobasso)

See also the list of [commits](https://gitlab.com/eduardobasso/game-of-life-react/-/commits/main/) to see everyone who participated in this project.