// Numbers
export const ZERO = 0;
export const ONE = 1;

// Strings
export const EMPTY = "";

// Board
export const BOARD_SIZE = 10;
export const BOARD_SIZE_MIN = 3;

// Time
export const DELAY = 0.5;
export const ONE_SECOND = 1000;