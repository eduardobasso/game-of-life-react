const API_URL = process.env.REACT_APP_API_URL;

export const BOARDS = `${API_URL}/boards`;
export const BOARD = `${API_URL}/board`;
export const BOARD_IMPORT = `${BOARD}/import`;
export const BOARD_NEXT = `${BOARD}/next`;