import { useState } from 'react';
import styled from 'styled-components';

import BoardForm from './BoardForm';
import IterationForm from './IterationForm';

import { RunningIterationsProvider } from '../contexts/RunningIterations';

const StyledSection = styled.section`
  margin: 20px 20px 32px;
  width: fit-content;
`;

export default function Controls() {
  const [runningIterations, setRunningIterations] = useState(false);

  return (
    <StyledSection>
      <h2 hidden>Game controls</h2>
      <RunningIterationsProvider value={[runningIterations, setRunningIterations]}>
        <BoardForm />
        <IterationForm />
      </RunningIterationsProvider>
    </StyledSection>
  );
}