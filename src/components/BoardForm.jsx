import { useEffect, useState } from 'react';

import FormRow from './FormRow';
import FormCol from './FormCol';
import FormButton from './FormButton';
import InputField from './InputField';
import SelectField from './SelectField';

import { useBoard } from '../contexts/Board';
import { useRunningIterations } from '../contexts/RunningIterations';

import { BOARD_SIZE, BOARD_SIZE_MIN, EMPTY, ZERO } from '../data/defaults';
import { BOARD, BOARDS } from '../data/endpoints';

export default function Controls() {
  const { board, updateBoard } = useBoard();
  const { runningIterations } = useRunningIterations();
  
  const [boardSize, setBoardSize] = useState(BOARD_SIZE);
  const [boardType, setBoardType] = useState(EMPTY);
  const [boardTypeOptions, setBoardTypeOptions] = useState([]);

  const changeBoardSize = (value) => {
    if (value < ZERO) setBoardSize(BOARD_SIZE_MIN);
    else setBoardSize(value);
  };

  const getBoardTypes = () => {
    fetch(BOARDS).then(response => response.json()).then(response => {
      setBoardTypeOptions(response);
      setBoardType(response[ZERO]);
    });
  };

  useEffect(getBoardTypes, []);

  const createBoard = (event) => {
    event.preventDefault();

    fetch(BOARD, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({ size: boardSize, type: boardType })
    }).then(response =>  response.json()).then(updateBoard);
  };
  
  return (
    <form noValidate id="boardForm" onSubmit={createBoard}>
      <FormRow>
        <FormCol>
          <InputField type="number" label="Size of board" value={boardSize} min="3" id="inputBoardSize" onChange={changeBoardSize} />
        </FormCol>
        <FormCol>
          <SelectField label="Board type (population)" value={boardType} id="selectBoardType" onChange={setBoardType}>
            {boardTypeOptions.map(option => <option key={option} value={option}>{option.toUpperCase()}</option>)}
            {boardTypeOptions.length === ZERO && <option defaultValue={EMPTY}>Loading options...</option>}
          </SelectField>
        </FormCol>
        <FormCol>
          <FormButton type="submit" disabled={runningIterations || Boolean(boardSize < BOARD_SIZE_MIN) || ! Boolean(boardType)} id="btnCreateOrResetBoard">
            {board.length === ZERO ? "Create board" : "Reset board"}
          </FormButton>
        </FormCol>
      </FormRow>
    </form>
  );
}