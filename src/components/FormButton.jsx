import styled from 'styled-components';

const StyledButton = styled.button`
  min-height: 38px;
  padding: 8px 12px;
  color: inherit;
  font-size: 14px;
  text-transform: uppercase;
  white-space: nowrap;
  cursor: pointer;
  &[disabled] {
    cursor: default;
  }
`;

export default function FormButton({ children, className, ...props }) {
  return (
    <StyledButton {...props}>
      {children}
    </StyledButton>
  );
}