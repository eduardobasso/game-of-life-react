import { Fragment } from 'react';
import styled from 'styled-components';

const StyledInput = styled.input`
  box-sizing: border-box;
  width: 202px;
  padding: 8px 12px;
  color: inherit;
  font-size: 16px;
`;
const StyledLabel = styled.label`
  display: block;
  margin-bottom: 6px;
  font-size: 14px;
`;

export default function InputField({ label, id, onChange, ...props }) {
  return (
    <Fragment>
      {label && <StyledLabel htmlFor={id}>{label}</StyledLabel>}
      <StyledInput id={id} onChange={event => onChange(event.target.value)} {...props} />
    </Fragment>
  );
}