import styled from 'styled-components';

const StyledButton = styled.button`
  display: flex;
  align-items: center;
  justify-content: center;
  margin: 1px;
  min-width: 32px;
  min-height: 32px;
  padding: 0;
  color: inherit;
  font-size: 16px;
  cursor: pointer;
`;

export default function BoardCell({ children, className, ...props }) {
  return (
    <StyledButton {...props}>
      {children}
    </StyledButton>
  );
}