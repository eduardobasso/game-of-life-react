import styled from 'styled-components';

import BoardCell from './BoardCell';

import { useBoard } from '../contexts/Board';

import { EMPTY } from '../data/defaults';
import { BOARD_IMPORT } from '../data/endpoints';

const StyledSection = styled.section`
  margin: 20px 20px 32px;
  width: fit-content;
`;
const StyledContainer = styled.div`
  display: inline-flex;
  flex-direction: row;
  margin: -1px;
`;
const StyledRow = styled.div`
  display: inline-flex;
  flex-direction: column;
`;

export default function Board() {
  const { board, updateBoard } = useBoard();

  const toggleCellState = (x, y) => {
    let newBoard = board;
    newBoard[x][y] = ! Boolean(newBoard[x][y]);

    fetch(BOARD_IMPORT, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({ board: newBoard })
    }).then(response =>  response.json()).then(updateBoard);
  };
  
  return (
    <StyledSection>
      <h2 hidden>Board</h2>
      <StyledContainer>
        {board.map((row, x) => (
          <StyledRow key={`row${x}`}>
            {row.map((col, y) => (
              <BoardCell key={`col${y}`} aria-label="Toggle populate" id={`btnToggleCellState-${x+1}-${y+1}`} onClick={() => toggleCellState(x, y)}>
                {Boolean(col) ? '😎' : EMPTY}
              </BoardCell>
            ))}
          </StyledRow>
        ))}
      </StyledContainer>
    </StyledSection>
  );
}