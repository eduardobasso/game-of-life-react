import { Fragment } from 'react';
import styled from 'styled-components';

const StyledSelect = styled.select`
  width: 202px;
  min-height: 38px;
  padding: 8px 10px;
  color: inherit;
  font-size: 14px;
  cursor: pointer;
`;
const StyledLabel = styled.label`
  display: block;
  mergin-bottom: 6px;
  font-size: 14px;
`;

export default function SelectField({ label, id, onChange, children, ...props }) {
  return (
    <Fragment>
      {label && <StyledLabel htmlFor={id}>{label}</StyledLabel>}
      <StyledSelect id={id} onChange={event => onChange(event.target.value)} {...props}>
        {children}
      </StyledSelect>
    </Fragment>
  );
}