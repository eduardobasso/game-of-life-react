import styled from 'styled-components';

const StyledRow = styled.div`
  display: flex;
  align-items: flex-end;
  margin: 0 -8px 16px;
`;

export default function FormRow({ children, ...props }) {
  return (
    <StyledRow {...props}>
      {children}
    </StyledRow>
  );
}