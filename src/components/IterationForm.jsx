import { useEffect, useRef, useState } from 'react';

import FormRow from './FormRow';
import FormCol from './FormCol';
import FormButton from './FormButton';
import InputField from './InputField';

import { useBoard } from '../contexts/Board';
import { useRunningIterations } from '../contexts/RunningIterations';

import { DELAY, ONE, ONE_SECOND, ZERO } from '../data/defaults';
import { BOARD_NEXT } from '../data/endpoints';

export default function Controls() {
  const { board, updateBoard } = useBoard();
  const { runningIterations, updateRunningIterations } = useRunningIterations();
  
  const [numberOfIterations, setNumberOfIterations] = useState(ONE);
  const [iterationDelay, setIterationDelay] = useState(DELAY);

  const iteration = useRef();
  const iterationsCount = useRef(ZERO);

  const changeNumberOfIterations = (value) => {
    if (value < ZERO) setNumberOfIterations(ZERO);
    else setNumberOfIterations(value);
  };

  const changeIterationDelay = (value) => {
    if (value < ZERO) setIterationDelay(ZERO);
    else setIterationDelay(value);
  };

  const runOrStopIterations = (event) => {
    event.preventDefault();

    updateRunningIterations(! runningIterations);
  };

  const runIterations = () => {
    runNextIteration();
    iteration.current = setInterval(runNextIteration, iterationDelay * ONE_SECOND);
  };

  const stopIterations = () => {
    clearInterval(iteration.current);
    iterationsCount.current = ZERO;
  };

  useEffect(() => {
    if (runningIterations) {
      runIterations();
    } else {
      stopIterations();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [runningIterations]);

  const runNextIteration = () => {
    fetch(BOARD_NEXT, {
      method: 'PUT'
    }).then(response => response.json()).then(updateBoard);

    if (runningIterations) {
      iterationsCount.current++;
      if (iterationsCount.current === +numberOfIterations) {
        updateRunningIterations(false);
      }
    }
  };
  
  return (
    <form noValidate id="iterationForm" onSubmit={runOrStopIterations}>
      <FormRow>
        <FormCol>
          <InputField type="number" label="# of iterations (0 for inifinite)" value={numberOfIterations} min="0" disabled={runningIterations} id="inputNumberOfIterations" onChange={changeNumberOfIterations} />
        </FormCol>
        <FormCol>
          <InputField type="number" label="Iteration delay (in seconds)" value={iterationDelay} min="0" step="0.5" disabled={runningIterations} id="inputIterationDelay" onChange={changeIterationDelay} />
        </FormCol>
        <FormCol>
          <FormButton type="submit" disabled={board.length === ZERO} id="btnRunOrStopIterations">{runningIterations ? "Stop" : "Run"}</FormButton>
        </FormCol>
        <FormCol>
          <FormButton type="button" disabled={runningIterations || board.length === ZERO} id="btnRunNextIteration" onClick={runNextIteration}>Run next iteration</FormButton>
        </FormCol>
      </FormRow>
    </form>
  );
}