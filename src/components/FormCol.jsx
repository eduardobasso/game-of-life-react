import styled from 'styled-components';

const StyledCol = styled.div`
  padding: 0 8px;
`;

export default function FormCol({ children, ...props }) {
  return (
    <StyledCol {...props}>
      {children}
    </StyledCol>
  );
}