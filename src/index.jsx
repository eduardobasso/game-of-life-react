import React, { StrictMode } from 'react';
import ReactDOM from 'react-dom';

import './assets/scss/base.scss';

import App from './components/App';

ReactDOM.render(
  <StrictMode>
    <App />
  </StrictMode>,
  document.getElementById('root')
);